package com.example.spysoundlocation;

import java.util.Timer;
import java.util.TimerTask;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class MainActivity extends Activity {

	private Button masterButton, slaveButton, syncButton;
	private ImageButton refreshButton, copyButton;

	Slave slave;
	Master master;
	Role role;
	EditText output_log;
	private String ip;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		masterButton = (Button) findViewById(R.id.master_button);
		slaveButton = (Button) findViewById(R.id.slave_button);
		syncButton = (Button) findViewById(R.id.sync_button);
		refreshButton = (ImageButton) findViewById(R.id.refreshButton);
		copyButton = (ImageButton) findViewById(R.id.copymasterip);
		output_log = (EditText) findViewById(R.id.output_log);

		masterButton.setOnClickListener(masterListener);
		slaveButton.setOnClickListener(slaveListener);
		syncButton.setOnClickListener(syncListener);
		refreshButton.setOnClickListener(refreshListener);
		copyButton.setOnClickListener(copyListener);

		refreshIp();

		// onCreate, we start listening sound on both master and slave
		// Audio ad = new Audio();

		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						if (role != null) {
							output_log.setText(role.getlog());
						} else {
							output_log.setText(R.string.choix_masterslave);
						}
					}
				});
			}
		}, 0, 500);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	private final OnClickListener slaveListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Log.d("VS", "Je suis l'esclave.");
			// Je dois streamer vers le master
			// slave = new Slave();
			try{
			if (role!=null)	role.close();
			
			Object obj = new Slave();
			role = (Role) obj;
			EditText ipmaitre_edittext = (EditText) findViewById(R.id.ipmaitre);
			role.setIpMaitre(ipmaitre_edittext.getText().toString());
			//role.setIpMaitre("192.168.0.12");
			role.run();
			}catch (Exception e) {
				Log.d("VS", "ErroronClick:" + e.toString());
			}
		}
	};
	
	private final OnClickListener syncListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Log.d("VS", "Synchronisation des temps");
			try{
				
				//EditText ipmaitre_edittext = (EditText) findViewById(R.id.ipmaitre);
				//role.setIpMaitre(ipmaitre_edittext.getText().toString());
				
				
			}catch (Exception e) {
				Log.d("VS", "ErroronClick:" + e.toString());
			}
		}
	};
	
	

	private final OnClickListener masterListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			Log.d("VS", "Recorder released");
			/*
			 * "Je suis le maître. Je dois ecouter sur une socket Je dois mixer
			 * les donne de la socket avec mon propre audio stream
			 */
			if (role!=null)	role.close();
			Object obj = new Master();
			role = (Role) obj;
			EditText ipmaitre_edittext = (EditText) findViewById(R.id.ipmaitre);
			ipmaitre_edittext.setText(ip);
			role.run();
		}
	};

	private final OnClickListener refreshListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			refreshIp();
		}
	};

	private final OnClickListener copyListener = new OnClickListener() {

		@Override
		public void onClick(View arg0) {
			EditText monipLabel = (EditText) findViewById(R.id.monip);
			EditText ipmaitreLabel = (EditText) findViewById(R.id.ipmaitre);
			ipmaitreLabel.setText(monipLabel.getText());
		}
	};

	protected void refreshIp() {
		WifiManager wifiMan = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiInfo wifiInf = wifiMan.getConnectionInfo();
		int ipAddress = wifiInf.getIpAddress();
		ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
				(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
				(ipAddress >> 24 & 0xff));
		EditText monipLabel = (EditText) findViewById(R.id.monip);
		monipLabel.setText(ip);
	}
}
