package com.example.spysoundlocation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder.AudioSource;
import android.media.ToneGenerator;
import android.util.Log;

public class Master extends Thread implements Role, Runnable {
	/*
	 * AudioInputStream audioInputStream; static AudioInputStream ais; static
	 * AudioFormat format;
	 */
	static boolean status = true;
	static int port = 50005;
	static int sampleRate = 44100;
	public String output_log = "";
	private String flushed = "";
	private AudioTrack track = null;
	private String pktFrom = ""; // This variable store the name of latest
									// sender
	private int ix = 0;
	private int datagramLength = 320;
	private boolean stopped = false;
	private int j = 0;
	short[] bufferrecorder,masterbufferrecorded;
	
	private DatagramSocket serverSocket = null;
	
	private int clapTime = 0; //delay between clap listen from slave and transmit to master
	private int ticTime = 0; //le tic time est le t0 (modulo le buffer) du son entendu par le master
	private boolean isClapped = false; //True if a clap have been send
	private boolean clapFound = false; //True when a clap have been validated
	private short[][] wideBufferrecorde;
	
	AudioRecord recorder;

	/**
	 * Give the thread high priority so that it's not canceled unexpectedly, and
	 * start it
	 */
	Master() {
		// android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
		// start();
	}

	public void run() {
		
		Log.d("spy", "demarage master run");
		Thread streamThread = new Thread(new Runnable() {
			@Override
			public void run() {
				Log.d("spy", "demarage master run");
				int N = AudioRecord.getMinBufferSize(8000,
						AudioFormat.CHANNEL_IN_MONO,
						AudioFormat.ENCODING_PCM_16BIT);
				recorder = new AudioRecord(AudioSource.MIC, 8000,
						AudioFormat.CHANNEL_IN_MONO,
						AudioFormat.ENCODING_PCM_16BIT, N * 10);

				track = new AudioTrack(AudioManager.STREAM_MUSIC, 8000,
						AudioFormat.CHANNEL_OUT_MONO,
						AudioFormat.ENCODING_PCM_16BIT, N * 10,
						AudioTrack.MODE_STREAM);
				recorder.startRecording();
				track.play();

				
				output_log += "Je suis le maître";

				// ( 1280 for 16 000Hz and 3584 for 44 100Hz (use
				// AudioRecord.getMinBufferSize(sampleRate, channelConfig,
				// audioFormat) to get the correct size)

				// format = new AudioFormat(sampleRate, 16, 1, true, false);
				connect_socket();
				wideBufferrecorde = new short[3000][160];
				while (!stopped) {
					// bufferrecorder is the current sound sample.
					//we loop for each bufferrecorder reads each turn of bufferrecorder.length
					
					bufferrecorder = new short[wideBufferrecorde[0].length];
					N = recorder.read(bufferrecorder, 0, bufferrecorder.length);
					ticTime += 1 % wideBufferrecorde.length;
					// we have to push him in a wider buffer to manage latency with the slave
					
					int x = ticTime;
					int y = wideBufferrecorde.length;
					
					int ct = mod(x,y);
					
					wideBufferrecorde[ ct ] = bufferrecorder;
					
					
					
					/*
					 * \ ATENTION! ON ECRIS 2X sur track...1 fois de trop! il
					 * faut diviser la taille du buffer!
					 * 
					 * //track.write(bufferrecorder, 0, bufferrecorder.length);
					 */
					byte[] receiveData = new byte[datagramLength];
					DatagramPacket receivePacket = new DatagramPacket(
							receiveData, receiveData.length);
					try {
						if(!isClapped && clapTime==0 && !clapFound){
							doClap();
							isClapped = true;
						}
						if (serverSocket.isClosed()){
							connect_socket();
						}
							serverSocket.receive(receivePacket);
							process(receivePacket);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						
					} finally {
						// recorder.stop();
						// recorder.release();
						// track.stop();
						// track.release();
						// serverSocket.close();
					}

				}
			}
		});
		streamThread.start();
	}
	
	private void connect_socket(){
		try {
			serverSocket = new DatagramSocket(50005);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void doClap(){
		// send the tone to the "alarm" stream (classic beeps go there) with 50% volume
		ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
		toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); 
		clapTime = 0;
	}

	public short bytesToShort(byte[] bytes) {
		return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getShort();
	}

	public byte[] shortToBytes(short value) {
		return ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN)
				.putShort(value).array();
	}
	
	private int mod(int x, int y)
	{
	    int result = x % y;
	    return result < 0? y - result : result;
	}

	public void process(DatagramPacket dpack) {
		// Log.d("spy", "demarage process");
		short[][] buffers = new short[256][160];
		

		// toSpeaker
		try {
			InetAddress inetAddr = dpack.getAddress();
			if (inetAddr == null) {
				output_log += "/!\\ Empty src.addr??";
				// return;
			}
			// output_log +=
			// "DRAMA: I have to do somethig, but dunno what yet!";
			// output_log += "Has received data from " + dpack.getAddress() +
			// ":" + dpack.getPort();
			/*
			 * if (dpack.getAddress().toString() != pktFrom.toString()){ pktFrom
			 * = dpack.getAddress().toString(); //output_log +=
			 * "Has received data from " + pktFrom; }
			 */

			byte[] resbytes = dpack.getData();
			
			int sum = 0;
			for (short byteVal:resbytes){
				sum += (short)byteVal;
			}
			output_log += sum+":";
			if(isClapped && !clapFound && sum>0){
				clapFound = true;
				ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
				toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_NETWORK_LITE, 200);
				output_log += "Clap listened!!! claptime:"+clapTime+" soundLevel:"+sum;
			}

			//int sum = Arrays.asList(buffer);
//					IntStream.of(); //resbytes.
			short[] buffer = buffers[ix++ % buffers.length];
			// output_log += Arrays.toString(resbytes).substring(0,10);
			// output_log += "Msg:"+resbytes.length+"\n";
			int x = ticTime - clapTime;
			int y = wideBufferrecorde[0].length;
			
			int ct = mod(x,y);
			output_log += "ct:"+clapTime+" x:"+x+",y="+y+",mod='" + ct + "'";
			masterbufferrecorded = wideBufferrecorde[ ct ];
			

			for (int i = 0; i < resbytes.length; i = i + 2) {
				// If clap have not been found but have been send,
				// 
				
				if(isClapped == true && clapFound == false){
					clapTime += 1;
				}
				// output_log +=
				// "boucle for "+i+" lenbuffer:"+buffer.length+"\n";
				byte[] buf = new byte[2];
				buf[0] = resbytes[i];
				buf[1] = resbytes[i + 1];

				//buffer[j % buffer.length] = (short) (bytesToShort(buf) + bufferrecorder[j]);
				buffer[j % buffer.length] = (short) (bytesToShort(buf) + masterbufferrecorded[j]);
				// j += 2;
				j += 1;

				if (j % buffer.length == 0) {
					// output_log += "j="+j;
					//output_log += Arrays.toString(buffer).substring(0, 10);
					track.write(buffer, 0, buffer.length);
					j = 0;
				}
			}
			// We print rode buffer
		} catch (Exception e) {
			output_log += "ErrorRead:" + e.toString();

			e.printStackTrace();
		}
	}
	
	private void setDelaySync(int delay){
		// Set the delay for sound traveling from slave mic to master speaker.
		clapTime = delay;
	}

	@Override
	public String getlog() {
		if (this.output_log != "") {
			flushed = this.output_log;
			this.output_log = "";
		}
		return flushed;
	}

	@Override
	public void setIpMaitre(String ipmaitre) {
		// TODO Auto-generated method stub

	}

	public void setStatus(boolean status) {
		// TODO Auto-generated method stub

	}

	public void close() {
		stopped = true;
		serverSocket.close();
	}

}