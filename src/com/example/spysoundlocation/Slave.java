package com.example.spysoundlocation;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.MediaRecorder.AudioSource;
import android.util.Log;

public class Slave extends Thread implements Role, Runnable {

	public byte[] buffer;
	public static DatagramSocket socket;
	AudioRecord recorder;
	private int sampleRate = 44100;   
	private int channelConfig = AudioFormat.CHANNEL_CONFIGURATION_MONO;    
	private int audioFormat = AudioFormat.ENCODING_PCM_16BIT;       
	int minBufSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat);
	private boolean status = true;
	public String output_log = "";
	private int datagramLength = 3584;
	private String flushed = "";


    private boolean stopped = false;
	private String ipmaitre;
    /**
     * Give the thread high priority so that it's not canceled unexpectedly, and start it
     */
    Slave()
    { 
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        start();
    }
	
	public short bytesToShort(byte[] bytes) {
	     return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getShort();
	}
	public byte[] shortToBytes(short value) {
	    return ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(value).array();
	}
	
	public void setIpMaitre(String ip){
		this.ipmaitre = ip;
	}

	public void run(){
		//start streaming
		final String ipmaitre = this.ipmaitre;
	    Thread streamThread = new Thread(new Runnable(){
			@Override
	        public void run()
	        {
	    		Log.i("Audio", "Running Audio Thread");
	            AudioRecord recorder = null;
	            AudioTrack track = null;
	            short[] buffer;
	            short[][]   buffers  = new short[256][160];
	            byte[][]   bytess  = new byte[256][160*2];
	            int ix = 0;
	            
	            try{
	
	                socket = new DatagramSocket();
	                
	                InetAddress IPAddress = InetAddress.getByName(ipmaitre);
	                
	                
	                Log.d("VS", "Socket Created");
	                output_log += "Socket Created\n";
	
	                //byte[] buffermin = new byte[minBufSize];
	
	                Log.d("VS","Buffer created of size " + minBufSize);
	                output_log += "Buffer created of size " + minBufSize+"\n";
	
	                Log.d("VS", "Address retrieved");
	                try
	                {
	                    int N = AudioRecord.getMinBufferSize(8000,AudioFormat.CHANNEL_IN_MONO,AudioFormat.ENCODING_PCM_16BIT);
	                    recorder = new AudioRecord(AudioSource.MIC, 8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, N*10);
	                    track = new AudioTrack(AudioManager.STREAM_MUSIC, 8000, 
	                            AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, N*10, AudioTrack.MODE_STREAM);
	                    recorder.startRecording();
	                    track.play();
	                    /*
	                     * Loops until something outside of this thread stops it.
	                     * Reads the data from the recorder and writes it to the audio track for playback.
	                     */
	                    DatagramPacket sendPacket;
	                    while(!stopped)
	                    { 
	                        // We get the actual buffer structure
	                        buffer = buffers[ix++ % buffers.length];
	                        // We record to buffer
	                        N = recorder.read(buffer,0,buffer.length);
	                        // We play back recorded sound
	                        track.write(buffer, 0, buffer.length);
	                        
	                        // We print rode buffer
	                        int sum = 0;
	            			for (short byteVal:buffer){
	            				sum += (short)byteVal;
	            			}
	            			output_log += sum+":";
	                        //output_log += Arrays.toString(buffer).substring(0,10);
	                        
	                        // We serialize all short[] to bytes[]
	                        byte[] buf = new byte[2];
	                        byte[] serialized_bytes = bytess[(ix++ % bytess.length)];
	                        for(int i=0;i<buffer.length-1;i+=1){
	            	    		buf = shortToBytes(buffer[i]);
	            	    		serialized_bytes[i*2]=buf[0];
	            	    		serialized_bytes[i*2+1]=buf[1];
	            	    	}
	                        
	                        sendPacket = new DatagramPacket(serialized_bytes, serialized_bytes.length, IPAddress, 50005);
		                    socket.send(sendPacket);
		                    // //output_log += new String(bytess[ix++ % buffers.length].toString());
		                    
		                    //output_log += "Msg:"+serialized_bytes.length+"\n";
		                    // output_log += Arrays.toString(serialized_bytes).substring(0,10);
		                    

		                     }
	                }
	                catch(Throwable x)
	                { 
	                    Log.w("Audio", "Error reading voice audio", x);
	                    output_log += "Error reading voice audio";
	                }
	                /*
	                 * Frees the thread's resources after the loop completes so that it can be run again
	                 */
	                finally
	                { 
	                    recorder.stop();
	                    recorder.release();
	                    track.stop();
	                    track.release();
	                    //socket.close();
	                }
	                Log.d("VS", "Recorder initialized");
	                output_log += "Recorder initialized\n";
	
	
	                //recorder.startRecording();

	
	            } catch(UnknownHostException e) {
	                Log.e("VS", "UnknownHostException");
	                output_log += e;
	            } catch (IOException e) {
	                Log.e("VS", "IOException");
	                e.printStackTrace();
	                output_log += e;
	            } catch (IllegalStateException e) {
	            	Log.e("VS", "IOException");
	                e.printStackTrace();
	                output_log += e;
	            }
	        }
	    });
	    streamThread.start();
	}
	
	@Override
	public String getlog() {
		if (this.output_log!=""){
			flushed = this.output_log;
			this.output_log = "";
		}
		return flushed;
	}
	
	public Slave getRole(){
		return this;
	}
    /**
     * Called from outside of the thread in order to stop the recording/playback loop
     */
    public void close()
    { 
         stopped = true;
    }

}
